import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {Exemple4Component} from "./view/exemple4/exemple4.component";
import {Exemple5Component} from "./view/exemple5/exemple5.component";
import {Exemple6Component} from "./view/exemple6/exemple6.component";
import {TotsusuariscomponentComponent} from "./view/totsusuariscomponent/totsusuariscomponent.component";
import {LogincomponentComponent} from "./view/logincomponent/logincomponent.component";
import {RegistercomponentComponent} from "./view/registercomponent/registercomponent.component";
import {Exemple7Component} from "./view/exemple7/exemple7.component";
import {InformecomponentComponent} from "./view/informecomponent/informecomponent.component";

const routes: Routes = [
  {path:'Exemple4', component:Exemple4Component},
  {path:'Exemple5', component:Exemple5Component},
  {path:'Exemple6', component:Exemple6Component},
  {path:'Tots Usuaris', component:TotsusuariscomponentComponent},
  {path:'login', component:LogincomponentComponent},
  {path:'register', component:RegistercomponentComponent},
  {path:'loginv2', component:Exemple7Component},
  {path:'informe', component:InformecomponentComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
