export class Songsclass {
  idSongs!:number;
  Nom!:string;
  Reproduccions!:Number;
  Recaptacio!:Number;

  constructor(Nom:string, Reproduccions:number, Recaptacio:number) {
    this.Nom=Nom;
    this.Reproduccions=Reproduccions;
    this.Recaptacio=Recaptacio;
  }
}
