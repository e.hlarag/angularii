import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Exemple4Component } from '../../../view/exemple4/exemple4.component';
import { Exemple5Component } from '../../../view/exemple5/exemple5.component';
import { Exemple6Component } from '../../../view/exemple6/exemple6.component';
import {ReactiveFormsModule} from "@angular/forms";
import { Exemple7Component } from '../../../view/exemple7/exemple7.component';
import {ValidarPasswordDirective} from "../../../view/exemple7/validar-password.directive";


@NgModule({
  declarations: [
    Exemple4Component,
    Exemple5Component,
    Exemple6Component,
    Exemple7Component,
    ValidarPasswordDirective
  ],
    imports: [
        CommonModule,
        ReactiveFormsModule
    ],
  exports: [
    Exemple4Component,
    Exemple5Component,
    Exemple6Component,
    Exemple7Component,
    ValidarPasswordDirective
  ]
})
export class Ex2Module { }
