import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TotsusuariscomponentComponent } from '../../../view/totsusuariscomponent/totsusuariscomponent.component';
import { LogincomponentComponent } from '../../../view/logincomponent/logincomponent.component';
import { RegistercomponentComponent } from '../../../view/registercomponent/registercomponent.component';
import {ReactiveFormsModule} from "@angular/forms";
import { PasswordValidatorDirective } from '../../../view/logincomponent/password-validator.directive';
import { InformecomponentComponent } from '../../../view/informecomponent/informecomponent.component';



@NgModule({
  declarations: [
    TotsusuariscomponentComponent,
    LogincomponentComponent,
    RegistercomponentComponent,
    PasswordValidatorDirective,
    InformecomponentComponent,
  ],
    imports: [
        CommonModule,
        ReactiveFormsModule
    ],
  exports: [
    TotsusuariscomponentComponent,
    LogincomponentComponent,
    RegistercomponentComponent,
    PasswordValidatorDirective,
    InformecomponentComponent,
  ]
})
export class ExercicisModulModule { }
