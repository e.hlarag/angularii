import { Injectable } from '@angular/core';
import {FormGroup} from "@angular/forms";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ConnectDBService {
  /**
   * @type string
   * @default http://localhost:3000
   */
  REST_API: string= 'http://localhost:3000';
  constructor(private httpclient:HttpClient) { }

  public getAllSongs():Observable<any>{
    return this.httpclient.get(`${this.REST_API}/songs`)
  }

  public getSongsRecap(form:FormGroup):Observable<any>{
    return this.httpclient.get(`${this.REST_API}/songs/rec`,{params:{reproduccions:form.controls['reproduccions'].value,recaptacio:form.controls['recaptacio'].value}});
  }

  public setSong(form:FormGroup):Observable<any>{
    return this.httpclient.post(`${this.REST_API}/songs/insert`, form.value)
  }
}
