import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Form, FormGroup} from "@angular/forms";

@Injectable({
  providedIn: 'root'
})
export class UsuarisServiceService {
  /**
   * @type string
   * @default http://localhost:3000
   */
  REST_API: string= 'http://localhost:3000';
  constructor(private httpclient:HttpClient) { }

  public getAllUsers():Observable<any>{
    return this.httpclient.get(`${this.REST_API}/users`)
  }
  public login(form:FormGroup):Observable<any>{
    return this.httpclient.get(`${this.REST_API}/user/rec`,{params:{correu:form.controls['nom'].value, password:form.controls['password'].value}})
  }

  public register(form:FormGroup):Observable<any>{
    return this.httpclient.post(`${this.REST_API}/user/insert`,form.value)

  }

  public usuariexisteix(form:FormGroup):Observable<boolean>{
    return this.httpclient.get<boolean>(`${this.REST_API}/user/exists`,{params:{correu:form.controls['nom'].value}})
  }
}
