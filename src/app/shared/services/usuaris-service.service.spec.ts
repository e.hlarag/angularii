import { TestBed } from '@angular/core/testing';

import { UsuarisServiceService } from './usuaris-service.service';

describe('UsuarisServiceService', () => {
  let service: UsuarisServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UsuarisServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
