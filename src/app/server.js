const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser=require ('body-parser');
const {find} = require("rxjs");
app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended:true
}));

// S'estableix un servidor que escolta pel port 3000
app.listen(3000, function (){
  console.log('Aplicació nodejs aixecada!!!');
});
// Es fa una API amb la url / que retorna un json
app.get('/', function (req, res){
  return res.send({error:false, message:"Hello"});
})
const client = require('mongodb').MongoClient;
const url = 'mongodb+srv://ehlarag:MuahDib@cluster0.jvqwrhq.mongodb.net/';
const dbName = 'albums';
const collection = 'usuaris_app'

// EXERCICI 2
app.get('/users',async (req,res)=>
  {
    let client2;
    try{
      client2= await client.connect(url);
      const db = client2.db(dbName);
      const users = await db.collection(collection).find().toArray();
      res.json(users);
    }catch (err){
      console.error("Error amb l'operació CRUD de l'API", err)
    } finally{
      if(client2){
        client2.close();
      }
    }
  }
);

//EXERCICI 3
app.get('/user/rec',async (req,res)=>
  {
    let correu=req.query.correu;
    let password=req.query.password
    console.log(correu);
    console.log(password);
    let client2;
    try{
      client2= await client.connect(url);
      const db = client2.db(dbName);
      const users = await db.collection(collection).
      find({
        'nom':correu,
        'password':password
      }).toArray()
      res.json(users);
    }catch (err){
      console.error("Error amb l'operació CRUD de l'API", err)
    } finally{
      if(client2){
        client2.close();
      }
    }
  }
);
app.get('/user/exists',async (req,res)=>
  {
    let correu=req.query.correu;
    console.log(correu);
    let client2;
    try{
      client2= await client.connect(url);
      const db = client2.db(dbName);
      const users = await db.collection(collection).
      find({
        'nom':correu,
      }).toArray()
      if (users.length===0){
        res.json(false)
      }else {
        res.json(true)
      }
    }catch (err){
      console.error("Error amb l'operació CRUD de l'API", err)
    } finally{
      if(client2){
        client2.close();
      }
    }
  }
);

//EXERCICI 4
app.post('/user/insert',async (req,res)=>
  {
    let client2;
    let nom=req.body.nom;
    let password=req.body.password;
    // Les dades es troben el body

    try{
      client2= await client.connect(url);
      const db = client2.db(dbName);
      let id = (await db.collection(collection).find().toArray()).length + 1;
      // Exemple de insert
      const user = await db.collection(collection).
      insertOne({'id': id,'nom':nom, 'password':password});

      res.json('Guardat!!!');
    }catch (err){
      console.error("Error amb l'operació CRUD de l'API", err)
    } finally{
      if(client2){
        client2.close();
      }
    }
  }
);

//EXERCICI 5
app.post('/user/update',async (req,res)=>
  {
    let client2;
    let id=req.body.id;
    let nom=req.body.nom;
    let password=req.body.password;
    // Les dades es troben el body

    try{
      client2= await client.connect(url);
      const db = client2.db(dbName);
      // Exemple de insert
      const user = await db.collection(collection).
      updateOne({'id': id},
        {$set:{
            nom:nom,
            password:password
          }}
      );

      res.json('Guardat!!!');
    }catch (err){
      console.error("Error amb l'operació CRUD de l'API", err)
    } finally{
      if(client2){
        client2.close();
      }
    }
  }
);

// Es fa una API amb una resposta de totes les cançons de la col·lecció de songs
app.get('/songs',async (req,res)=>
  {
    let client2;
    try{
      client2= await client.connect(url);
      const db = client2.db(dbName);
      const songs = await db.collection('songs').find().toArray();
      res.json(songs);
    }catch (err){
      console.error("Error amb l'operació CRUD de l'API", err)
    } finally{
      if(client2){
        client2.close();
      }
    }
  }
);

// Es fa una API amb una resposta amb un filtre
app.get('/songs/rec',async (req,res)=>
  {
    let client2;
    let reproduccions = Number(req.query.reproduccions);
    let recaptacio = Number(req.query.recaptacio);
    try{
      client2= await client.connect(url);
      const db = client2.db(dbName);
      // toArray() -> Això és per retornar tots els elements coincidents
      const songs = await db.collection('songs').
      find({'Reproduccions':{$gte:reproduccions}, 'Recaptacio':{$gte:recaptacio}}).toArray();
      // next -> Això és per retornar el primer element elements coicident
      /*const songs = await db.collection(collection).
      find({'Reproduccions':{$gte:reproduccions}, 'Recaptacio':{$gte:recaptacio}}).next();*/
      res.json(songs);
    }catch (err){
      console.error("Error amb l'operació CRUD de l'API", err)
    } finally{
      if(client2){
        client2.close();
      }
    }
  }
);
// Exemple de Post
app.post('/songs/insert',async (req,res)=>
  {
    let client2;
    // Les dades es troben el body
    let nom = req.body.nom;
    let reproduccions = Number(req.body.reproduccions);
    let recaptacio = Number(req.body.recaptacio);
    console.log(nom)
    console.log(reproduccions)
    console.log(recaptacio)
    try{
      client2= await client.connect(url);
      const db = client2.db(dbName);
      let id = (await db.collection('songs').find().toArray()).length + 1;
      // Exemple de insert
      const songs = await db.collection('songs').
      insertOne({'id': id,'Nom':nom, 'Reproduccions':reproduccions, 'Recaptacio':recaptacio});
      console.log(id);
      console.log(songs)
      res.json('Guardat!!!');
    }catch (err){
      console.error("Error amb l'operació CRUD de l'API", err)
    } finally{
      if(client2){
        client2.close();
      }
    }
  }
);


