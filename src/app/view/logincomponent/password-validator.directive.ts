import {Directive, Input} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors, ValidatorFn} from "@angular/forms";

@Directive({
  selector: '[appPasswordValidator]',
  providers:[{provide:NG_VALIDATORS,
  useExisting:PasswordValidatorDirective, multi:true}]
})
export class PasswordValidatorDirective {
  @Input('appName') passwordStrength:boolean=false;
  @Input('appName') nomStrength:boolean=false;

  validate(control:AbstractControl):ValidationErrors | null{
    return this.passwordStrength ? passwordValidator()(control):null
  }
 validate2(control:AbstractControl):ValidationErrors | null{
    return this.nomStrength ? emailValidator()(control):null
  }

  constructor() { }

}
export function emailValidator():ValidatorFn{
  return (control:AbstractControl):ValidationErrors | null =>{
    const value=control.value;
    if (!value) return null;
    const hasEmail:boolean=/@ies-sabadell\.cat$/.test(value);
    const nomValid:boolean=hasEmail;
    return !nomValid ? {nomStrength:true}:null;
;
  }
}
export function passwordValidator():ValidatorFn{
  return (control:AbstractControl):ValidationErrors | null =>{
    const value=control.value;
    if (!value) return null;
    const hasUpperCase:boolean=/[A-Z]+/.test(value);
    const hasLowerCase:boolean=/[a-z]+/.test(value);
    const hasNumeric:boolean=/[0-9]+/.test(value);
    const hasSpecialChar:boolean=/[!$%^&*()_+|~=`{}\[\]:";'<>?,./\\-]/.test(value)
    const passwordValid:boolean=hasUpperCase && hasLowerCase && hasNumeric && hasSpecialChar;
    return !passwordValid ? {passwordStrength:true}:null;

  }
}
