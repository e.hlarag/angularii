import {Component, OnInit} from '@angular/core';
import {UsuarisServiceService} from "../../shared/services/usuaris-service.service";
import {Usersclass} from "../../shared/classes/usersclass";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {emailValidator, passwordValidator} from "./password-validator.directive";

@Component({
  selector: 'app-logincomponent',
  templateUrl: './logincomponent.component.html',
  styleUrl: './logincomponent.component.css'
})
export class LogincomponentComponent  implements OnInit{
  constructor(private connectusers:UsuarisServiceService) {}
  data!:Usersclass[];
  userForm!:FormGroup;
  message!:string;

  ngOnInit() {
    this.userForm=new FormGroup({
      nom:new FormControl('', [Validators.required, Validators.minLength(7), emailValidator()]),
      password:new FormControl('', [Validators.required, Validators.minLength(8), passwordValidator()])
    });
  }

  login(){
    this.connectusers.login(this.userForm)
      .subscribe(res=>{
        console.log(res);
        if (res.length==0){
          this.data=[];
          this.message="Login incorrecte";
        }else{
          this.data=res;
          console.log(this.data[0]);
          this.message="L'usuari "+this.data[0].nom+" s'ha logejat de manera correcta!!!"
        }
      })
  }

}
