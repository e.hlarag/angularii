import { Component } from '@angular/core';
import {Songsclass} from "../../shared/classes/songsclass";
import {ConnectDBService} from "../../shared/services/connect-db.service";

@Component({
  selector: 'app-exemple4',
  templateUrl: './exemple4.component.html',
  styleUrl: './exemple4.component.css'
})
export class Exemple4Component {
  constructor(private connectbd:ConnectDBService) {}
  data!:Songsclass[];
  getSongs(){
    this.connectbd.getAllSongs().subscribe(res=>{
      console.log(res);
      if (res.length==0){
        this.data=[];
      }else{
        this.data=res;
      }
    })
  }
}
