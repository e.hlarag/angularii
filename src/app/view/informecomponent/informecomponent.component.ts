import {Component, ElementRef, ViewChild} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {ConnectDBService} from "../../shared/services/connect-db.service";
import {Songsclass} from "../../shared/classes/songsclass";
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts"
pdfMake.vfs=pdfFonts.pdfMake.vfs;
import htmlToPdfmake from "html-to-pdfmake";
import jsPDF from "jspdf";

@Component({
  selector: 'app-informecomponent',
  templateUrl: './informecomponent.component.html',
  styleUrl: './informecomponent.component.css'
})
export class InformecomponentComponent {
  constructor(private connectbdsongs:ConnectDBService) {}
  data!:Songsclass[];
  dataindata!:Songsclass[];
  form:FormControl= new FormControl('', [Validators.required])
  @ViewChild('pdfTable') pdfTable!:ElementRef;
  getPdf(){
    const doc = new jsPDF();
    const pdfTable=this.pdfTable.nativeElement;
    var html=htmlToPdfmake(pdfTable.innerHTML);
    const documentDefinition={content:html}
    pdfMake.createPdf(documentDefinition).open();
  }

  getUsers(){
    this.connectbdsongs.getAllSongs().subscribe(res=>{
      console.log(res);
      if (res.length==0){
        this.data=[];
      }else if (this.dataindata==null){
        this.data=res;
      }else{
        this.data=this.dataindata
      }
    })
  }
  filtrar(){
    console.log(this.dataindata)
    console.log(this.form.value)
    this.dataindata=this.data.filter(value => value?.Nom?.includes(this.form.value))
  }
}
