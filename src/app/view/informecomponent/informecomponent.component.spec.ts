import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformecomponentComponent } from './informecomponent.component';

describe('InformecomponentComponent', () => {
  let component: InformecomponentComponent;
  let fixture: ComponentFixture<InformecomponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InformecomponentComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(InformecomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
