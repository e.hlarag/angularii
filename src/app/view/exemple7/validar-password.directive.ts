import {Directive, Input} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors, ValidatorFn} from "@angular/forms";

@Directive({
  selector: '[appValidarPassword]',
  providers:[{provide:NG_VALIDATORS,
  useExisting:ValidarPasswordDirective, multi:true}]
})
export class ValidarPasswordDirective {
  @Input('appName') passwordStrength=false;

  validate(control:AbstractControl):ValidationErrors | null {
    return this.passwordStrength ? passwordValidator()(control)
      :null;
  }


  constructor() { }

}

export function passwordValidator():ValidatorFn{
  return (control:AbstractControl):ValidationErrors | null =>{
    const value=control.value; //amb aixo agafes el valor de l'input
    if (!value){
      return null;
    }
    const hasUpperCase:boolean=/[A-Z]+/.test(value);
    const hasLowerCase:boolean=/[a-z]+/.test(value);
    const hasNumeric:boolean=/[0-9]+/.test(value);
    const passwordValid:boolean=hasUpperCase && hasLowerCase && hasNumeric;
    return !passwordValid ? {passwordStrength:true}:null; //si no es valida la validacio s'activa, si no no.
  };
}
