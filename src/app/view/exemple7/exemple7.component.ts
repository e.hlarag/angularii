import {Component, OnInit} from '@angular/core';
import {ConnectDBService} from "../../shared/services/connect-db.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Usersclass} from "../../shared/classes/usersclass";
import {UsuarisServiceService} from "../../shared/services/usuaris-service.service";
import {passwordValidator} from "./validar-password.directive";

@Component({
  selector: 'app-exemple7',
  templateUrl: './exemple7.component.html',
  styleUrl: './exemple7.component.css'
})
export class Exemple7Component implements OnInit{
  constructor(private connectbd:UsuarisServiceService) {}
    loginForm!:FormGroup;
    users:Usersclass[]=[];
    message!:string;

    ngOnInit():void {
      this.loginForm = new FormGroup({
        nom: new FormControl('', [Validators.required, Validators.minLength(3)]),
        password: new FormControl('', [Validators.required, Validators.minLength(5), passwordValidator()])
      })
    }

    login():void{
      this.connectbd.login(this.loginForm)
        .subscribe(res=>{
          console.log(res);
          if (res.length==0){
            this.users=[];
            this.message="Login incorrecte"
          }else{
            this.users=res;
            console.log(this.users[0])
            this.message="L'usuari "+this.users[0].nom+" s'ha logejat de manera correcta!!!"
          }
        })
    }
}
