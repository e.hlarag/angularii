import { Component } from '@angular/core';
import {UsuarisServiceService} from "../../shared/services/usuaris-service.service";
import {Usersclass} from "../../shared/classes/usersclass";

@Component({
  selector: 'app-totsusuariscomponent',
  templateUrl: './totsusuariscomponent.component.html',
  styleUrl: './totsusuariscomponent.component.css'
})
export class TotsusuariscomponentComponent {
  constructor(private connectusers:UsuarisServiceService) {}
  data!:Usersclass[];

  getUsers(){
    this.connectusers.getAllUsers().subscribe(res=>{
      console.log(res);
      if (res.length==0){
        this.data=[];
      }else{
        this.data=res;
      }
    })
  }

}
