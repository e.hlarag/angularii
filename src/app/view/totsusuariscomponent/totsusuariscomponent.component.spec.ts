import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TotsusuariscomponentComponent } from './totsusuariscomponent.component';

describe('TotsusuariscomponentComponent', () => {
  let component: TotsusuariscomponentComponent;
  let fixture: ComponentFixture<TotsusuariscomponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TotsusuariscomponentComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TotsusuariscomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
