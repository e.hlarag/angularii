import {Component, OnInit} from '@angular/core';
import {ConnectDBService} from "../../shared/services/connect-db.service";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-exemple6',
  templateUrl: './exemple6.component.html',
  styleUrl: './exemple6.component.css'
})
export class Exemple6Component implements OnInit{

  constructor(private connectbd:ConnectDBService) {}
  songForm!:FormGroup;
  resultat!:string

  ngOnInit() {
    this.songForm=new FormGroup({
      nom:new FormControl(''),
      reproduccions:new FormControl(0),
      recaptacio: new FormControl(0)
    })
  }

  Guardar(){
    this.connectbd.setSong(this.songForm)
      .subscribe(res=>{
        console.log(res);
        this.resultat=res;
      })
  }

}
