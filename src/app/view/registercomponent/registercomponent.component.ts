import {Component, OnInit} from '@angular/core';
import {UsuarisServiceService} from "../../shared/services/usuaris-service.service";
import {Form, FormControl, FormGroup, Validators} from "@angular/forms";
import {emailValidator, passwordValidator} from "../logincomponent/password-validator.directive";
import {Usersclass} from "../../shared/classes/usersclass";

@Component({
  selector: 'app-registercomponent',
  templateUrl: './registercomponent.component.html',
  styleUrl: './registercomponent.component.css'
})
export class RegistercomponentComponent implements OnInit{
  constructor(private connectusers:UsuarisServiceService) {
  }
  userForm!:FormGroup;
  resultat!:string;
  data!:Usersclass[];

  ngOnInit() {
    this.userForm=new FormGroup({
      nom:new FormControl('', [Validators.required, Validators.minLength(7), emailValidator()]),
      password:new FormControl('', [Validators.required, Validators.minLength(8), passwordValidator()])
    });
  }
  register(){
    this.connectusers.usuariexisteix(this.userForm)
      .subscribe(res=>{
        console.log(res)
        if (res){
          this.resultat="Error: Aquest nom ja existeix, siusplau escull un altre."
        }else{
          this.connectusers.register(this.userForm)
            .subscribe(res=>{
              console.log(res);
              this.resultat=res;
            })
        }
      })
  }


}
