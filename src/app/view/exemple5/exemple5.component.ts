import {Component, OnInit} from '@angular/core';
import {ConnectDBService} from "../../shared/services/connect-db.service";
import {Songsclass} from "../../shared/classes/songsclass";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-exemple5',
  templateUrl: './exemple5.component.html',
  styleUrl: './exemple5.component.css'
})
export class Exemple5Component implements OnInit{
  constructor(private connectbd:ConnectDBService) {}
  data!:Songsclass[];
  songForm!:FormGroup;

  ngOnInit() {
    this.songForm=new FormGroup({
      reproduccions:new FormControl(0),
      recaptacio: new FormControl(0)
    });
  }
  Recuperar(){
    this.connectbd.getSongsRecap(this.songForm)
      .subscribe(res=>{
        console.log(res);
        if (res.length==0){
          this.data=[];
        }else{
          this.data=res;
        }
      });
  }
}
